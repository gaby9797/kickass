from SockClassNEW import SockClassNEW
from Tkinter import *


def main():
	client = SockClassNEW("127.0.0.1" , 15000)
	client.connectToServer()
	window = Tk()

	count=0
	prev={0,0}
	while count<1000:
		x = window.winfo_pointerx()
		y = window.winfo_pointery()
		curr={x,y}
		client.recvInfo()
		client.sendInfo(str(x))
		client.recvInfo()
		client.sendInfo(str(y))
		client.recvInfo()
		if(curr==prev):
			count=count+1
		else:
			count=0
			prev=curr
		if(count == 1000):
			client.sendInfo(str(-1))
		else:
			client.sendInfo(str(0))

	client.closeConnection()
	print "Connection Closed!"


if __name__ == '__main__':
	main()

