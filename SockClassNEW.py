import socket


class SockClassNEW: #client
	def __init__(self , myHost , port):
		self.myHost = myHost
		self.port = port
		self.serverPort = 10000
		self.oppontHost = None
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def connectToServer(self):
		print "***************************"
		print "trying to connect to server"
		self.myHost = socket.gethostname()
		self.sock.connect((self.myHost , self.serverPort))
		print self.sock.recv(1024)
		self.sock.send("connection confirmed succesfully!")
		flag = self.sock.recv(1024)
		if flag == "True": #no one is listening in the server
			self.sock.send(str(self.port))
			print self.sock.recv(1024)
			self.sock.close()
			self.sockListening()

		else: #there is a listener
			self.sock.send("Client is ready to receive the listeners info.")
			self.oppontHost = self.sock.recv(1024)
			print "received oppont hostname"
			self.sock.send("a")
			p = int(self.sock.recv(1024))
			print "received port"
			self.port = p
			self.sock.send("a") 
			print "oppont info: " , self.oppontHost , " , " , self.port
			self.sock.close()
			self.startConnection()


	def sockListening(self) : #listening 
		print "**************************"
		self.sock = socket.socket()
		self.sock.bind((self.myHost,self.port))

		self.sock.listen(5)
		print "started listening"
		c, addr = self.sock.accept()
		print 'Got connection from', addr
		c.send('Thank you for your connecting')
		self.sock = c

	def startConnection(self) : #sending request
		print "**************************"
		self.sock = socket.socket()
		self.sock.connect((self.oppontHost , self.port))
		print self.sock.recv(1024)

	def sendInfo(self , info): #send string to oppont
		self.sock.send(info)

	def closeConnection(self): #close the socket
		self.sock.close()

	def recvInfo(self):  #receive string from oppont
		return self.sock.recv(1024)







