from Tkinter import *
import time
from SockClassNEW import SockClassNEW

class App (Tk):
    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        self.overrideredirect(True)
        self.geometry("{0}x{1}+0+0".format(self.winfo_screenwidth(),self.winfo_screenheight()))
        self.C=Canvas(self,bg="blue",height=600,width=700,bd=5)
        self.C.pack()

    def printOnWindow(self,x1,y1 , connection):
        connection.sendInfo("OK")
        x2 = int(connection.recvInfo())
        connection.sendInfo("OK")
        y2 = int(connection.recvInfo())
        connection.sendInfo("OK")
        flag = int(connection.recvInfo())
        if flag != -1:
            print x2,y2,self.winfo_screenwidth()/2-350 
            line = self.C.create_line(( -self.winfo_screenwidth()/2+350 + x1 ), y1 , (-self.winfo_screenwidth()/2+350+x2) ,y2, fill="red" , width = 5 )
            self.C.pack()
            self.after(1,self.printOnWindow,x2,y2 , connection)
        else:
            connection.closeConnection()
            print "Connection Closed!"
            self.destroy()
        
client = SockClassNEW("127.0.0.1" , 15000)
client.connectToServer()
app=App()
app.printOnWindow(app.winfo_pointerx(),app.winfo_pointery(),client)
app.mainloop()